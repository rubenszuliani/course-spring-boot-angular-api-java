package project.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * Classe de configuração Para autenticação
 * Obrigatorio anotar com EnableWebSecurity
 * A anotação ja vem com o @Configuration dentro dele, mas o cara do curso gosta de colocar a anotaação kkk
 * Para bater o olho e saber que e uma classe de configuração
 * Alteramos a classe de SecurityConfig para ResourceServerConfig, para conseguimos trabalhar com oauth2
 *
 * @EnableResourceServer - Oauth2
 * Em vez de extendermos WebSecurityConfigurerAdapter vamos extender ResourceServerConfigurerAdapter
 *
 */
@Configuration
@EnableWebSecurity
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    /**
     * WebSecurityConfigurerAdapter ganha alguns metodos que podemos sobreescrever para customizas as configurações
     * Oauth2
     * Como extendemos ResourceServerConfigurerAdapter o AuthenticationManagerBuilder é injetado agora
     * nesse caso em vez de utilizarmos @Override vamos utilizar o Autowired
     */
    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        /**
         * Vamos informar da onde iremos validar o usuario e senha
         * em produção colocariamos que vai ser buscado do banco de dados
         * Mas como e um teste iremos informar que vai ser da memoria
         * Informaremos qual o usuario e senha, e por padrão temos que informar qual a ROLES
         * ROLES = Permissão
         */
        auth.inMemoryAuthentication()
                .withUser("admin").password("admin").roles("ROLE");
    }

    /**
     * Metodo que faz a verificação das autorizações de nossas requisições
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        /**
         * Vamos informar nessa linha de codigo que qualquer requisição de nossa api precisam estar autenticado
         * ou seja precisamos que o usuario e senha seja enviado
         * .antMatchers("/categorias").permitAll() - informo que essa url qualquer pessoa pode acessar
         * O resto das requisições precisa de autenticação
         * httpBasic() - tipo  de autenticação
         * .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); Nossa api Rest não mantem
         * Estado de nada, nesse caso nao teremos sessão no servidor
         * .csrf() crosaite request forjai para caso você consiga fazer um javascript injection dentro de um servico web
         * nesse caso nao teremos um servico web na nossa aplicação ai desabilita.
         * //.and().httpBasic() - oauth 2 não e basic
         */
        http.authorizeRequests()
                .antMatchers("/categorias").permitAll()
                .anyRequest().authenticated()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable();

    }

    /**
     * Vamos sobreescrever esse metodo para deixar o servidor stetless
     * Não manteremos estado nenhum no nosso servidor
     * @param resources
     * @throws Exception
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.stateless(true);
    }
}
