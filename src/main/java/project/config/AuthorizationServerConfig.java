package project.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * Classe de configuração de autorização das API
 * Diferente do Resource Server config onde configuramos para aplicação ser obrigado a passar um token
 * e essa classe que enviara a autorização para o cliente
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    /**
     * 1º coisa - Injetar o autentication manager
     * vai ser que vai gerenciar a autenticação para a gente pegando do ResourceServeConfigo o usuario e senha
     * admin admin
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * Configurando o cliente - quem o usuario esta usando
     * Vamos sobreescrever o metodo ClientDetailsServiceConfigurer
     * Vamos autorizar o cliente a acessar o authorization serve
     *
     * Nessa função que informo se sera buscada do banco de dados ou em memoria
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        /**
         * inMemory - o usuario e senha sera pego da memoria
         * withClient - qual o nome do cliente
         * secret - passoword do cliente
         * scopes - qual o scopo desse cliente - Podemos limitar o acesso desse cliente nesse caso
         * Podemos definir escopo diferentes por cada cliente
         * essas strings somos nos que definimos, depos precisamos tratalas no acesso aos metodos
         *
         * authorizedGrantTypes - typo de autorização da aplicação :
         * A pessoa vai digitar o usuario e senha na tela do angular e o angular vai enviar para nosso sistema
         * para pegar o acess token
         *
         * accessTokenValiditySeconds - quantos segundos esse token vai ficar ativo - 1800/60 = 30 minutos
         *
         * refresh_token - vamos adicionar o refresh token para caso o token expire o navegador solicite um novo
         *
         * refreshTokenValiditySeconds vamos dar o tempo de vida do refresh token, iremos colocar 1 dia para expirar
         *
         *
         */
        clients.inMemory()
                .withClient("angular")
                .secret("@ngula@r0")
                .scopes("read", "write")
                .authorizedGrantTypes("password", "refresh_token")
                .accessTokenValiditySeconds(40)
                .refreshTokenValiditySeconds(3600 * 24);
    }

    /**
     *
     * @param endpoints
     * @throws Exception
     * Sobreescrever o metodo AuthorizationServerEndpointsConfigurer
     *
     *
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        /**
         * COnfigurar os endpoints
         * tokenStore - armazenando o tokem - iremos armazenar em um tokenStore
         * O angular vai vir aqui buscar um token = string, depos vai enviar essa string de volta para poder acessar
         * nossa API - por isso o mesmo deve estar armazenado em algum lugar para verificarmos sua validade
         * authenticationManager - usuaro e senha vindos da aplicação
         *
         * accessTokenConverter - adicionando jwt no nosso projeto
         * .reuseRefreshTokens(false) - sempre que pedirmos um novo acess_token usando refreshtoken, um novo
         * refresh_token sera enviado, nesse caso enquanto o usuario estiver utilizando a aplicação ele não se deslog
         * sempre que pedimos um novo reflesh_token nao sera utilizado o mesmo, visto que a propriedade esta false
         */
        endpoints
                .tokenStore(tokenStore())
                .accessTokenConverter(accessTokenConverter())
                .reuseRefreshTokens(false)
                .authenticationManager(authenticationManager);
    }

    /**
     * funçao que faz a conversao do token
     * como vamos trabalhar com jwt nao ficara nada na memoria, e sim no token
     * @return
     *
     * Quem precisar de um acesstokenConverte consegue recuperar atraves do Bean
     */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
        accessTokenConverter.setSigningKey("algaworks");//senha que valida o token, ultima parte dele
        return accessTokenConverter;
    }

    /**
     *
     * @return
     * Função que armazena o Token
     */
    @Bean
    public TokenStore tokenStore(){
        /**
         * Retornar o token em memoria
         */
        //return new InMemoryTokenStore();

        /**
         * Tonem Store passa a ser um JWT
         * no construtor dele precisamos de um accessTokenConverter
         */
        return new JwtTokenStore(accessTokenConverter());

    }
}
