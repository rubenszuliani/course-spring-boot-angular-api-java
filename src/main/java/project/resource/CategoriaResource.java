package project.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import project.event.RecursoCriadoEvent;
import project.model.Categoria;
import project.repository.CategoriaRepository;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

/**
 * Controller
 * Classe que vai expor tudo que esteja relacionado ao recurso categoria
 *
 *RestController -- retorno ja vai ser convetido em Json
 * RequestMapping -- Mapeamento da requisicao
 */

@RestController
@RequestMapping("categorias")
public class CategoriaResource {

    /**
     * Vamos disparar um evento no Spring
     * publisher - publicador de evento de aplicação - Classe ApplicationEvent
     * @return
     */
    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private CategoriaRepository categoriaRepository;

    @GetMapping
    public List<Categoria> listar(){
        return categoriaRepository.findAll();
    }

    /**
     * RequestBody -- transformando o Json que esta vindo na requisiçao Post em um objeto
     * @param categoria
     */
    @PostMapping
    //@ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Categoria> criar(@Valid @RequestBody Categoria categoria, HttpServletResponse response){
        //Salvando a categoria, e pegando o codigo categoriaSalva
        Categoria categoriaSalva = categoriaRepository.save(categoria);

        /**
         * Vamos criar o evemto para inserir no header da resposta
         * source This - Objeto que gerou o evento nesse caso, essa classe PessoaResource
         * response - e o HttpServeletResponse passado na função
                * Id = codigo da pessoa salva
                */
        publisher.publishEvent(new RecursoCriadoEvent(this, response, categoriaSalva.getCodigo()));


        /**
         * Devolvendo o recurso que foi criado
         * com o response entity created ja estou passando o status e no body a informação
         * Como nao temos mas a URI quando inserimos o Event, precisamos musar o retorno
         * return ResponseEntity.created(uri).body(categoriaSalva);
         * passaremos o HttpStatus.CREATED
         */
        return ResponseEntity.status(HttpStatus.CREATED).body(categoriaSalva);
    }

    /**
     * PathVariable -- como precisamos pegar o codigo da URL precisamos utilizar essa anotação
     * @param codigo sera o numero passado na, url vai para o parametro codigo da função
     * @return
     */
    @GetMapping("/{codigo}")
    public Categoria buscarPeloCodigo(@PathVariable Long codigo){
        return categoriaRepository.findOne(codigo);
    }



}
