package project.resource;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import project.event.RecursoCriadoEvent;
import project.model.Pessoa;
import project.repository.PessoaRepository;
import project.service.PessoaService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("pessoas")
public class PessoaResource {

    @Autowired
    PessoaRepository pessoaRepository;

    /**
     * Vamos disparar um evento no Spring
     * publisher - publicador de evento de aplicação - Classe ApplicationEvent
     * @return
     */
    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private PessoaService pessoaService;

    @GetMapping
    public List<Pessoa> listar(){
        return pessoaRepository.findAll();
    }

    /**
     * PathVariable -- como precisamos pegar o codigo da URL precisamos utilizar essa anotação
     * @param id sera o numero passado na, url vai para o parametro codigo da função
     * @return
     */
    @GetMapping("/{id}")
    public Pessoa buscarPeloCodigo (@PathVariable Long id){
        return pessoaRepository.findOne(id);
    }

    /**
     *
     * @param pessoa
     * @param response
     * @return
     */
    @PostMapping
    public ResponseEntity<Pessoa> criar(@Valid @RequestBody Pessoa pessoa, HttpServletResponse response){

        //Salvando os dados, e pegando o codigo gerado
        Pessoa pessoaSalva = pessoaRepository.save(pessoa);

        /**
         * Vamos criar o evemto para inserir no header da resposta
         * source This - Objeto que gerou o evento nesse caso, essa classe PessoaResource
         * response - e o HttpServeletResponse passado na função
         * Id = codigo da pessoa salva
         */
        publisher.publishEvent(new RecursoCriadoEvent(this, response, pessoaSalva.getId()));

        /**
         * Devolvendo o recurso que foi criado
         * com o response entity created ja estou passando o status e no body a informação
         * Como nao temos mas a URI quando inserimos o Event, precisamos musar o retorno
         * return ResponseEntity.created(uri).body(pessoaSalva);
         * passaremos o HttpStatus.CREATED
         */
        return ResponseEntity.status(HttpStatus.CREATED).body(pessoaSalva);

    }

    /**
     * Função que Deleta o registro
     * Retorno é um codigo 204 - sucesso mas não tenho nada para te retornar, por isso a função e void
     * @param codigo
     */
    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT) //retornando o codigo 204
    public void remover(@PathVariable Long codigo){
        pessoaRepository.delete(codigo);
    }

    /**
     * Fazendo a atualização dos dados da pessoa com put
     * Atualização total
     * Vamos receber a URL / ID
     * Vamos receber os dados da pessoa para atualizar
     */
    @PutMapping("/{id}")
    public ResponseEntity<Pessoa> atualizar(@PathVariable Long id, @Valid @RequestBody Pessoa pessoa){
        /**
         * Chamando a classe de servico para buscar a Pessoa salva e alterar os dados
         */
        Pessoa pessoaSalva = pessoaService.atualizar(id, pessoa);

        /**
         * Retornando um response entity ok com os dados da pessoa salva
         */
        return ResponseEntity.ok(pessoaSalva);
    }

    /**
     * Implementando a atualização parcial,
     * Nesse caso campo ativo
     * Atualização parcial de uma propriedade nos expoem ela, colocando o campo no final da url de acesso
     * nesse caso nao iremos retornar nada
     * @RequestBody Boolean ativo - ja estamos pegando a propriedade ativo
     * Bastando passar como true / false que ja vai funcionar
     * RequestBody boolean e obrigatorio, se passarmos um valor que diferente nosso ExceptionHandle vai tratar
     */
    @PutMapping("/{id}/ativo")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizarPropriedadeAtivo(@PathVariable Long id, @RequestBody Boolean ativo){

        /**
         * Como teremos regra de negocio, vamos chamar a classe pessoaService para nos ajudar
         */
        pessoaService.atualizarPropriedadeAtivo(id, ativo);
    }

}
