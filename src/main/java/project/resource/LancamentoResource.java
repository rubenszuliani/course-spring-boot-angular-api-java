package project.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.event.RecursoCriadoEvent;
import project.exceptionhandler.AlgamoneyExceptionHandler;
import project.model.Lancamento;
import project.repository.LancamentoRepository;
import project.repository.filter.LancamentoFilter;
import project.service.LancamentoService;
import project.service.exception.PessoaInexistenteOuInativaException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("lancamentos")
public class LancamentoResource {

    @Autowired
    LancamentoRepository lancamentoRepository;

    /**
     * Quando criamos uma classe de serviço, não utilizamos mas a classe Repository na classe Resource
     * Utilizamos Repository dentro da Service para salvar o oBjeto caso tenha regra de negocio
     */
    @Autowired
    LancamentoService lancamentoService;

    @Autowired
    ApplicationEventPublisher publisher;

    @Autowired
    private MessageSource messageSource;

    /**
     * Função que me traz uma lista de Lancamentos
     * @return
     *
     * pageable - sao os parametros de paginação  size a page
     * Vamos retornar em vez de lista uma PAginação. Ou varias paginas de lancamento
     */
    @GetMapping
    public Page<Lancamento> pesquisar(LancamentoFilter lancamentoFilter, Pageable pageable){
        /**
         * Vamos passar os parametros pageable na hora de fazer a consulta
         */
        return lancamentoRepository.filtrar(lancamentoFilter, pageable);
    }

    @GetMapping("/{codigo}")
    public Lancamento buscarPeloCodigo(@PathVariable Long codigo){
        return lancamentoRepository.findOne(codigo);
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long codigo){
        lancamentoRepository.delete(codigo);
    }

    @PostMapping
    public ResponseEntity<Lancamento> criar(@Valid @RequestBody Lancamento lancamento, HttpServletResponse response){
        /**
         * 1º salvar a pessoa no banco de dados
         */
        Lancamento lancamentoSalvo = lancamentoService.salvar(lancamento);

        /**
         * Usar o Evento para enviar a url de retorno para o header da resposta
         */
        publisher.publishEvent(new RecursoCriadoEvent(this, response, lancamentoSalvo.getCodigo()));

        /**
         * Devolvendo o recurso que foi criado
         * com o response entity created ja estou passando o status e no body a informação
         * Como nao temos mas a URI quando inserimos o Event, precisamos musar o retorno
         * return ResponseEntity.created(uri).body(lancamentoSAlvo);
         * passaremos o HttpStatus.CREATED
         */

        return ResponseEntity.status(HttpStatus.CREATED).body(lancamentoSalvo);
    }

    /**
     * Essa classe de exceção é para caso quisermos criar uma dentro do Resource para uma função expecifica
     * Não fazemos isso em projetos deprodução apenas para estudo.
     * Criaremos uma classe dentro de ExceptionHandler ou outro pacote, e colocaremos as exceções customizadas dentro dele
     * @param ex
     * @return
     */
    @ExceptionHandler({PessoaInexistenteOuInativaException.class})
    public ResponseEntity<Object> handlePessoaInexistenteOuInativaException(PessoaInexistenteOuInativaException ex){
        String mensagemUsuario = messageSource.getMessage("pessoa.inexistente-ou-inativa", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<AlgamoneyExceptionHandler.Erro> erros = Arrays.asList(new AlgamoneyExceptionHandler.Erro(mensagemUsuario, mensagemDesenvolvedor));
        return ResponseEntity.badRequest().body(erros);
    }

}
