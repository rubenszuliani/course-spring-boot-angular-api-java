package project.token;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Classe que intercepta o token para trabalhar encima dele antes da resposta http
 * Processador depos do refresh toke ter sido criado
 *
 * ResponseBodyAdvice - Interface que informo qual objeto quero implementar
 *
 * OAuth2AccessToken - Tipo do dado que quero que seja interceptado quando tiver retornado o corpo da resposta
 *
 * ex: se quiser interceptar a categoria, bastaria eu ir em categoria Resource Pegar qual a Entidade - no caso categoria
 * e informar ele no ResponseBodyAdvice
 *
 * OAuth2AccessToken - e o objeto de retorno da classe Oauth2
 *
 * para saber como funciona esse tipo de classe precisamos ler o codigo fonte do spring security
 */
@ControllerAdvice
public class RefreshTokenPostProcessor implements ResponseBodyAdvice<OAuth2AccessToken> {

    /**
     *
     * @param returnType
     * @param converterType
     * @return
     */
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        /**
         * Quando esse metodo ser executado e verificarmos que é postAcessToken, sabemos que conseguimos pegar
         * o corpo da requisição
         */
        return returnType.getMethod().getName().equals("postAccessToken");
    }

    /**
     * Regra de negocio vai ficar nessa classe
     * So vamos executar o metodo beforeBodyWrite quando o supports() nos retorna true
     *
     * @param body
     * @param returnType
     * @param selectedContentType
     * @param selectedConverterType
     * @param request
     * @param response
     * @return
     */
    @Override
    public OAuth2AccessToken beforeBodyWrite(OAuth2AccessToken body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        /**
         * Nesse momento temos o refreshtoken nessa variavel
         */
        String refreshToken = body.getRefreshToken().getValue();

        /**
         * Para adicionar o refresh token no cookie precisamos do HttpRequest e HttpResponse
         * Nesse caso ja recebemos essa infomação nessa função como request e response mas como ServerHttpRequest e ServerHttpResponse
         * nesse caso precisamos converter eles
         */
        HttpServletRequest req = ((ServletServerHttpRequest) request).getServletRequest();

        HttpServletResponse resp = ((ServletServerHttpResponse) response).getServletResponse();

        /**
         * Vamos fazer um casting do OAuth2cessToken para pegar ele paa remover do body
         * Fazendo esse casting no token teremos o metodo setRefreshToken e poderemos passar ele para null
         * Conseguimos saber que e essa a classe olhando dentro do codigo font do spring Security oauth 2
         */
        DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) body;



        /**
         * * Agora iremos adicionar o refresh token no cookie
         */
        adicionarRefreshTokenNoCookie(refreshToken, req, resp);
        /**
         * Remover o Refresh token do Body - resposta da aplicação
         */
        removerRefreshTokenDoBody(token);


        /**
         * Retornando a resposta ja configurada
         */
        return body;
    }

    /**
     *
     * @param token
     */
    private void removerRefreshTokenDoBody(DefaultOAuth2AccessToken token) {
        token.setRefreshToken(null);
    }

    /**
     * Metodo para adicionar o refresh token no cookie
     * @param refreshToken
     * @param req - pegar o conextPath = caminho da aplicação
     * @param resp - adicionar o cookie na resposta
     */
    private void adicionarRefreshTokenNoCookie(String refreshToken, HttpServletRequest req, HttpServletResponse resp) {
        /**
         * Criando um cookie HTTP
         * refreshToken - nome do cookie que iremos dar
         */
        Cookie refreshTokenCookie = new Cookie("refreshToken", refreshToken);
        /**
         * dando alguma propriedades para o cookie
         */
        refreshTokenCookie.setHttpOnly(true);//Apenas um cookie de HTTP
        refreshTokenCookie.setSecure(false);// TODO: funcionar apenas em ambiente seguro HTTPS Mudar em produção
        refreshTokenCookie.setPath(req.getContextPath() + "/oauth/token");//para qual caminho esse cookie deve ser enviado para o browser
        refreshTokenCookie.setMaxAge(2592000);//em quanto tempo esse cookie vai expirar em dias 30
        resp.addCookie(refreshTokenCookie); // adicionar o cookie na resposta


    }

}
