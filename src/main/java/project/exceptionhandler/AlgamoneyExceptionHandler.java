package project.exceptionhandler;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ControllerAdvice
public class AlgamoneyExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Objeto de mensagem do Spring para quando subimos a aplicação
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * handleHttpMessageNotReadable - vai capturar as mensagens que não conseguiu ler
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {

        /**
         * Buscando a mensagem do arquivo Messages.properties
         * messageSource - Objeto do Spring
         * getMessage()
         * 1° parametro e o codigo no messages.properties
         * 2º parametro  parametro extra
         * 3º parametro Locaio portugues/ingles - Pegamos o Locaio corrente
         */
        String mensagemUsuario = messageSource.getMessage("mensagem.invalida", null, LocaleContextHolder.getLocale());

        /**
         * String que pega a mensagem para enviar para o  desenvolvedor
         * No caso pegamos a Exceção
         * Se na exceção tiver a Causa, pegamos e enviamos ela, se nao ja enviamoa a String que vem na exception
         */
        String mensagemDesenvolvedor = ex.getCause() != null ? ex.getCause().toString() : ex.toString();


        /**
         * Criando uma Lista com os erros
         */
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));

        /**
         * dentro do ResponseEntityExceptionHandler existe um metodo chamado handleExceptionInternal
         * Nessa esseção poderemos passar um Body, Corpo, que queremos ao inves de deixamos o padrão
         * Podemos passar outro status para requisição EX HttpStatus.BAD_REQUEST
         * outros headers
         */
        return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * FUnção que verifica se os argumentos passados de parametro são validos
     * De acordo com o que você informar na Entidade @Valid
     * Vamos capturar quando der exceção
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {

        /**
         * Vamos chamar a função para criar a lista de erros
         * Passando o getBindingResult, resultado do erro por parametro
         */
        List<Erro> erros = criarListadeErros(ex.getBindingResult());

        return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
        //return super.handleMethodArgumentNotValid(ex, headers, status, request);
    }

    /**
     * Quando crio uma função para tratar uma exceção eu coloco Handle + nome da exceção
     * handleEmptyResultDataAccessException - função que faz o tratamento quando tentamos deletar algo que
     * não existe na tabela do banco de dados
     * RuntimeException ex - A função recebe a exceção como parametro
     * Nessa primeira verção nao inserimos a RuntimeException ex
     * Só informaremos que o retorno é HttpStatus.NOT_FOUND-  não existe o recurso que esta tentando acessar
     */
    @ExceptionHandler({EmptyResultDataAccessException.class})
    //@ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> handleEmptyResultDataAccessException(WebRequest request, RuntimeException ex){

        String mensagemUsuario = messageSource.getMessage("recurso.naoencontrado", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));


        /**
         * Fazendo um retorno para o Usuario que cair na exception
         * ex - Exceptions vindas por parametro
         * erros
         * new HttpHeaders() - Temos que criar um novo header por não ter como passar pela função
         * HttpStatus.NOT_FOUND  - Status que vamos responder caso de o erro
         * request - Request vindo da função
         */
        return handleExceptionInternal(ex, erros, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    /**
     * Caso algum dado não é enviado ao nosso end-point, vamos sobreescrever a função DataIntegrityViolationException
     * para que nao retorne erro de servidor
     */

    @ExceptionHandler({ DataIntegrityViolationException.class })
    public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request){
        String mensagemUsuario = messageSource.getMessage("recurso.operacao-nao-permitida", null, LocaleContextHolder.getLocale());

        /**
         * Utilizando a Dependecia org.apache.commons
         * podemos utilizar a classe ExceptionUtils.getRootCauseMessage(ex) - commons.lang3
         * Ele vai dar uma mensagem da causa raiz  da exceção
         * Mostrando para o desenvolvedor uma mensagem mas descritiva, mostrando qual e o problema
         */
        String mensagemDesenvolvedor = ExceptionUtils.getRootCauseMessage(ex);

        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));


        /**
         * Fazendo um retorno para o Usuario que cair na exception
         * ex - Exceptions vindas por parametro
         * erros
         * new HttpHeaders() - Temos que criar um novo header por não ter como passar pela função
         * HttpStatus.NOT_FOUND  - Status que vamos responder caso de o erro
         * request - Request vindo da função
         */
        return handleExceptionInternal(ex, erros, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }





    /**
     * Vamos criar uma lista de erro, com todos os campos que será obrigatorio
     * @return
     * BindingResult - onde termos a lista com todos os erros
     */
    private List<Erro> criarListadeErros(BindingResult bindingResult){
        List<Erro> erros = new ArrayList<>();

        /**
         * Nesse for, estamos pedinddo para o java, nos dar todos os erros que acontece nos campos mapeados na entidade
         */
        for (FieldError fieldError : bindingResult.getFieldErrors()){

            /**
             * Setar a mensagem para o usuario pegando o camppo eo local
             */
            String mensagemUsuario = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
            /**
             * Pegar a mensagem de erro e dar um toString para pegar apenas os dados
             */
            String mensagemDesenvolvedor = fieldError.toString();
            erros.add(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        }
        return erros;
    }

    /**
     * Como utilizaremos a Classe apenas aqui
     * Vamos criar essa gambiarra de classe para poder passar as duas mensagens na mesma requisição
     */
    public static class Erro{

        private String mensagemUsuario;
        private String mensagemDesenvolvedor;

        /**
         * Para acessar essa classe, criamos o metodo contrutor, para ser obrigatorio passar os parametros
         * @param mensagemUsuario
         * @param mensagemDesenvolvedor
         */
        public Erro(String mensagemUsuario, String mensagemDesenvolvedor) {
            this.mensagemUsuario = mensagemUsuario;
            this.mensagemDesenvolvedor = mensagemDesenvolvedor;
        }

        public String getMensagemUsuario() {
            return mensagemUsuario;
        }

        public String getMensagemDesenvolvedor() {
            return mensagemDesenvolvedor;
        }
    }
}
