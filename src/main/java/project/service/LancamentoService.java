package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.model.Lancamento;
import project.model.Pessoa;
import project.repository.LancamentoRepository;
import project.repository.PessoaRepository;
import project.service.exception.PessoaInexistenteOuInativaException;

@Service
public class LancamentoService {

    /**
     * Dentro da entidade pessoa, vai me retornar se a mesma esta ativa ou nao
     */
    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private LancamentoRepository lancamentoRepository;

    /**
     * Função que verificara se a pessoa esta ativa antes de fazer um novo lancamento
     * @param lancamento
     * @return
     */
    public Lancamento salvar(Lancamento lancamento) {
        /**
         * Verificar se a pessoa esta ativa ou nao
         */
        Pessoa pessoa = pessoaRepository.findOne(lancamento.getPessoa().getId());
        /**
         * Se não existir a pessoa, ou a mesma estiver inativa, lança uma exceção
         */
        if(pessoa == null || pessoa.isInativo()){
            /**
             * Criamos uma classe de exceção dentro de servico/exception por que na nossa classe de exeção principal
             * criamos exceções que qualquer controlador pode acessar
             * Vamos pegar a mensagem da classe messages.properties
             */
            throw new PessoaInexistenteOuInativaException();
        }

        /**
         * Caso passe pela validação o mesmo ira salvar o lancamento e retornar o objeto salvo
         */
        return lancamentoRepository.save(lancamento);


    }
}
