package project.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import project.model.Pessoa;
import project.repository.PessoaRepository;

/**
 * Classe de servico do Expring, sera um componemte da aplicação
 * Essa e a classe onde ficara a regra de negocio da aplicação, que será enviado para a classe Resource/controller
 */

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    public Pessoa atualizar(Long id, Pessoa pessoa){

        /**
         * Chamar a função de buscar a pessoa
         */
        Pessoa pessoaSalva = buscarPessoaPeloCodigo(id);

        /**
         * Vamos Usar uma classe utilitaria do Spring para copiar os atributos pessoa, passada na função
         * Para o objeto vindo do banco de dados
         * copyProperties
         * 1° As informações que quero pegar desse objeto - Pessoa que estamos recebendo vinda do cliente
         * 2º pessoaSalva inseri nesse objeto vindo do banco - Pessoa que esta no banco de dados
         * 3° Ignorando o codigo, pos vem null
         */
        BeanUtils.copyProperties(pessoa, pessoaSalva, "id");

        /**
         * Apos atualizar os dados do Objeto pessoa precisamos salvar no banco de dados
         */
        return pessoaRepository.save(pessoaSalva);
    }

    public void atualizarPropriedadeAtivo(Long id, Boolean ativo) {
        /**
         * Chamar a função de buscar a pessoa
         */
        Pessoa pessoaSalva = buscarPessoaPeloCodigo(id);

        /**
         * Vamos pegar o objeto que veio de nossa busca e passar a propriedade ativo da função
         */
        pessoaSalva.setAtivo(ativo);

        /**
         * Depos disso chamamos o repository, função save, com a entidade alterada
         */
        pessoaRepository.save(pessoaSalva);

    }

    /**
     * Verifica no banco se a Pessoa existe, caso contrario da uma exceção
     * @param id
     * @return
     */
    private Pessoa buscarPessoaPeloCodigo(Long id) {
        /**
         * Vamos buscar a pessoa no banco de dados com o id passado
         */
        Pessoa pessoaSalva = pessoaRepository.findOne(id);

        /**
         * Lançando uma exceção caso nao encontre registro
         */
        if(pessoaSalva == null){
            /**
             * No metofo contrutor espera 1 paramentro - A função esperava pelo menos 1 elemento e foi retornado 0
             */
            throw new EmptyResultDataAccessException(1);
        }
        return pessoaSalva;
    }
}
