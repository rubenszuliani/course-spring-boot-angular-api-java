package project.event;

import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletResponse;

/**
 * Classe de dispado de evento
 * Essa classe e o evento, quando chamarmos precisamos passar os metodos construtores
 */
public class RecursoCriadoEvent extends ApplicationEvent {

    private static final long serialVersionUID = 1L;

    /**
     * Variaveis que utilizaremos no metodo construtor
     */
    private HttpServletResponse response;
    private Long codigo;

     /**
     * Create a new ApplicationEvent.
      * QUando extendemos essa classe precisamos colocar o version e o construtor com Super
     *  Alem do source podemos receber outros objetos
      *  HttpServletResponse response - resposta do Http quando salvo a informacao
      *  Long codigo - codigo para adicionar no header
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public RecursoCriadoEvent(Object source, HttpServletResponse response, Long codigo) {
        super(source);
        this.response = response;
        this.codigo = codigo;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public Long getCodigo() {
        return codigo;
    }
}
