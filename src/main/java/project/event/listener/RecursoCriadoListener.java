package project.event.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import project.event.RecursoCriadoEvent;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;

/**
 * vamos criar o listnei desse evento
 * Listener e o cara que ouve o evento
 * O mesmo vai implementar uma classe implements ApplicationListener<RecursoCriadoEvent>
 * passamos quem o mesmo deve escutar no caso RecursoCriadoEvent que criamos anteriormente
 * Precisamos informar que a classe é um @Componemt para ser um componemte do ispring
 * E imprimentar o metodo onApplicationEvent() que a interface informa
 */
@Component
public class RecursoCriadoListener implements ApplicationListener<RecursoCriadoEvent> {
    @Override
    public void onApplicationEvent(RecursoCriadoEvent recursoCriadoEvent) {

        /**
         * Nesse caso iremos pegar o response do metodo e o codigo
         */
        HttpServletResponse response = recursoCriadoEvent.getResponse();
        Long codigo = recursoCriadoEvent.getCodigo();

        /**
         * Chamar a função que faz o adicionamento
         */
        adicionarHeaderLocation(response, codigo);

    }

    /**
     * Metodo que pega as informações para adicionar no Header da resposta
     * @param response
     * @param codigo
     */
    private void adicionarHeaderLocation(HttpServletResponse response, Long codigo) {

        /**
         * Usar a classe Builder, pegando da RequestUri atual,colocando um Path chamado codigo, dar um bild passando o codigo
         * vai ser devolvida uma URI
         */
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}")
                .buildAndExpand(codigo).toUri();
        /**
         * Agora setamos o HeaderLocation com a uri
         */
        response.setHeader("Location", uri.toASCIIString());
    }
}
