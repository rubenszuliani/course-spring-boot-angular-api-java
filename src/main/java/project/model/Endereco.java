package project.model;

import javax.persistence.Embeddable;

/**
 * Uma Entidade Embeddable, e uma entidade que não possui tabela no banco de dados
 * A mesma é um complemento da classe que chama ela.
 * Isso serve para que uma classe não fique tão grande
 * Na tabela do banco a mesma coluna contem todos os codigos dessa classe e da classe principal
 *
 */
@Embeddable
public class Endereco {

    /**
     * Parametros opicional
     */
    private String logradouro;
    private String numero;
    private String complemento;
    private String bairro;
    private String cep;
    private String cidade;
    private String estado;


    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
