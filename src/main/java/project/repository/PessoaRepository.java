package project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import project.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
}
