package project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import project.model.Lancamento;
import project.repository.lancamento.LancamentoRepositoryQuery;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>, LancamentoRepositoryQuery {
}
