package project.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import project.model.Categoria;

/**
 * Classe que utiliza os metodos de consulta no banco de dados
 */
public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
