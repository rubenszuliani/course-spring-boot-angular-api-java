package project.repository.lancamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import project.model.Lancamento;
import project.repository.filter.LancamentoFilter;

import java.util.List;

public interface LancamentoRepositoryQuery {

    /**
     * Vamos receber o Lancamento Filter nessa função
     * Vamos adicionar a paginação, então será necessario informar o Pageable
     * Para utilizar a paginação precisamos mudar o tipo de retorno do metodo para Page
     * @param lancamentoFilter
     * @return
     */
    public Page<Lancamento> filtrar(LancamentoFilter lancamentoFilter, Pageable pageable);
}
