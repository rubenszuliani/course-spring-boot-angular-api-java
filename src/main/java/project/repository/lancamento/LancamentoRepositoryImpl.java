package project.repository.lancamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import project.model.Lancamento;
import project.repository.filter.LancamentoFilter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * no mesmo pacote Lancamento criamos uma classe LancamentoRepositoryImpl (Implementação)
 * nessa classe coloco que a mesma implements LancamentoRepositoryQuery
 * ai nessa classe chamamos o metodo filtrar para implementalo
 * creio que esse curso seja bem antigo, provavelmente a maneiras mas Faceis de fazer.
 *
 */
public class LancamentoRepositoryImpl implements LancamentoRepositoryQuery{

    /**
     * Temos que instanciar o Entity Meneger para podermos trabalha com a consulta
     */
    @PersistenceContext
    private EntityManager manager;


    /**
     * Implementação da interface LancamentoRepository
     * @param lancamentoFilter
     * @return
     */
    @Override
    public Page<Lancamento> filtrar(LancamentoFilter lancamentoFilter, Pageable pageable) {

        /**
         * Utilizaremos a criteria do JPA
         */

        /**
         * 1º criarmos a CriteriaBuilder do Jpa
         */
        CriteriaBuilder builder = manager.getCriteriaBuilder();

        /**
         *2º Agora podemos criar as Query sempre importando javax.persistence.criteria
         */
        CriteriaQuery<Lancamento> criteria = builder.createQuery(Lancamento.class);


        /**
         * 3° Podemos adicionar os filtros aqui no meio
         */

        /**
         * 3 - 1 - Criarmos um Root da entidade que estamos trabalhando
         */
        Root<Lancamento> root = criteria.from(Lancamento.class);

        /**
         * 3 - 2 - criar um Array de Predicates
         *
         * Para criar as restricoes vamos fazer em uma classe separada,
         * Passando de parametros o filtro que queremos, o builder e o root
         */
        Predicate[] predicates = criarRestricoes(lancamentoFilter, builder, root);

        /**
         * Criar as restricoes e depos no criteria.where passar um array de predicates
         */
        criteria.where(predicates);





        /**
         * 4° Criaremos a query passando a criteria da classe
         */
        TypedQuery<Lancamento> query = manager.createQuery(criteria);

        /**
         * 5º Retornaremos a Query
         */
        //return query.getResultList();

        /**
         * Vamos fazer um metodo que adiciona as restricoes de paginacao
         */
        adicionarRestricoesDePaginacao(query, pageable);

        /**
         * Como estamos fazendo a paginação, precisamos alterar o retorno do metodo
         * 1º passamos o conteudo, result list
         * 2º pageable
         * 3° total de elementos
         */
        return new PageImpl<>(query.getResultList(), pageable, total(lancamentoFilter));

    }


    /**
     * Adicionar as restricoes de Paginação
     * @param query
     * @param pageable
     */
    private void adicionarRestricoesDePaginacao(TypedQuery<Lancamento> query, Pageable pageable) {
        /**
         * Vamos pegar a pagina atual
         */
        int paginaAtual = pageable.getPageNumber();
        /**
         * Total de Registro por pagina
         */
        int totalRegistroPorPagina = pageable.getPageSize();
        /**
         * Qual o primeiro registro que tenho que mostrar
         * Se estou na pagina 2 e eu tenho 3 registro mostrando por pagina,
         * 2 * 3 = 6
         * O primeiro registro que tenho que mostrar na minha consulta e o 6
         */
        int primeiroRegistroDaPagina = paginaAtual * totalRegistroPorPagina;

        /**
         * Informar qual o primeiro registro que tenho que mostrar
         */
        query.setFirstResult(primeiroRegistroDaPagina);
        /**
         * Informar o total de registro
         */
        query.setMaxResults(totalRegistroPorPagina);
    }

    /**
     * Calcular a quantidade de elementos totais para o nosso filtro
     * @param lancamentoFilter
     * @return
     */
    private Long total(LancamentoFilter lancamentoFilter) {
        /**
         * Total de Lancamentos preciamos criar uma nova criteria
         */
        CriteriaBuilder builder = manager.getCriteriaBuilder();
        /**
         * Vamos retornar um Long
         */
        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);

        Root<Lancamento> root = criteria.from(Lancamento.class);

        /**
         * Adicionar um filtro
         */
        Predicate[] predicates = criarRestricoes(lancamentoFilter, builder, root);
        criteria.where(predicates);

        /**
         * Fazendo um select na criteria para contat quantos registros tem
         */
        criteria.select(builder.count(root));

        return manager.createQuery(criteria).getSingleResult();
    }

    /**
     * Essa função é que nos retornara a lista de predicates
     * @param lancamentoFilter
     * @param builder
     * @param root
     * @return
     */
    private Predicate[] criarRestricoes(LancamentoFilter lancamentoFilter, CriteriaBuilder builder, Root<Lancamento> root) {
            /**
             * Essa Lista de precicate sera criada conforme foi passado na filtro que e a propriedade lancamentoFilter
             * Como a Lista de Predicate pode ter tamanho variado, vamos criar uma lista de Predicate
             */
            List<Predicate> predicates = new ArrayList<>();

            /**
             * Esse filtro e opicional, podemos passar a data de lancamento ou nao, nome ou nao, ou o mesmo vir null
             * Então se algum parametro vier vamos criar as Criterias
             * No caso NO SQL É COMO SE FOCE WHERE DESCRICAO LIKE = ""
             */

            /**
             * Utilizando a classe do spring utils para verificar se o resultado vem vazio
             * se a string nao vier vazia, adiciona essa descricao no predicate
             */
            if(!StringUtils.isEmpty(lancamentoFilter.getDescricao())){
                /**
                 * Fazendo a consulta em descricao
                 * Como temos que utilizar o like, no JPA e builder.like tudo em minusculo
                 */
                /**
                 * SQL
                 * WHERE DESCRICAO LIKE '%texto aqui%'
                 */
                predicates.add(builder.like(//PARA FAZER IM LIKE TEMOS QUE UTIIZAR O BUILDER
                        builder.lower(root.get("descricao")),//qual o campo que queremos pesquisar
                        "%" + lancamentoFilter.getDescricao().toLowerCase() + "%"//passando a pesquisa em letra minuscula
                ));

            }

            /**
             * Fazer o mesmo para data de vencimento de
             * Data de vencimento maior ou igua >=
             */
            if(lancamentoFilter.getDataVencimentoDe() != null){
                predicates.add(
                        builder.greaterThanOrEqualTo(root.get("dataVencimento"), lancamentoFilter.getDataVencimentoDe()));
            }

            /**
             * Fazer o mesmo para data de vencimento ate
             * Tem que ser menor ou igual por isso utilizamos o lessThanOrEqualTo
             * Lembrando que o filtro nao tem os dois campos dentro da entidade apensa o dataVencimento
             */
            if(lancamentoFilter.getDataVencimentoAte() != null){
                predicates.add(
                        builder.lessThanOrEqualTo(root.get("dataVencimento"), lancamentoFilter.getDataVencimentoAte()));
            }

            /**
             * Na hora de retornar transformo o predicates em um array de predicates
             * predicates.size() - insiro o tamanho que o predicate tem
             */
            return predicates.toArray(new Predicate[predicates.size()]);

    }
}
